How to use:

1. npm i  => To install all the required modules(you don't need to install jest, supertest or jest-html-reporter explicitly).

2. In the jest.config.js there are two commented parts, when you run testcases for the first time you have to uncomment part 1, then after that every time when you run testcases secound part must be uncommented and the first part must be commented.

3. Use token.js file for token storage and token retrieval.

4. You can get all the api collection from Emerge.postman_collection.
